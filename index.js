const getRandomValues = (buf) => {
    if (typeof process !== 'undefined') {
        if (!(buf instanceof Uint8Array)) {
            throw new TypeError('Expected Uint8Array.');
        }
        const nodeCrypto = require('crypto');
        const bytes = nodeCrypto.randomBytes(buf.length);
        buf.set(bytes);
        return buf;
    }

    if (window.crypto && window.crypto.getRandomValues) {
        return window.crypto.getRandomValues(buf);
      }
    
      if (window.msCrypto && window.msCrypto.getRandomValues) {
        return window.msCrypto.getRandomValues(buf);
      }
    
      throw new Error('No secure random number generator available.');
};

module.exports = getRandomValues;